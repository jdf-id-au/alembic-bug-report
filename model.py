from sqlalchemy import create_engine, Table, Column, Integer, Enum
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///bug.db')

Base = declarative_base()

class Annoying(Base):
	__tablename__ = 'annoying'

	id = Column(Integer, primary_key=True)
	first_enum = Column(Enum(*list('abcdefg')))
	second_enum = Column(Enum(*list('12345')))