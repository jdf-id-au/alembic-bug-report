`batch_alter_table` doesn't retain `CHECK` constraint for unchanged `Enum`.

Steps to reproduce (Python 3.5.1):

```
mkdir venv
pyvenv venv
source venv/bin/activate
pip install -r requirements.txt
alembic history
alembic upgrade a4c
sqlite3 bug.db .schema # output in a4c-schema
alembic upgrade 3ec
sqlite3 bug.db .schema # output in 3ec-schema
```

Note how `CHECK` constraint for `second_enum` disappeared.