"""first_enum extra options

Revision ID: 3ecf03c1514c
Revises: a4c9c2b1d1f0
Create Date: 2016-12-10 19:49:27.349317

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3ecf03c1514c'
down_revision = 'a4c9c2b1d1f0'
branch_labels = None
depends_on = None


def upgrade():
	with op.batch_alter_table('annoying') as batch_op:
		batch_op.alter_column('first_enum', type_=sa.Enum(*list('abcdefg')), existing_type=(sa.Enum(*list('abcde'))))

def downgrade():
	pass